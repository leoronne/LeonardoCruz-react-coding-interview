import { useCallback, useState } from "react";

import APIClient from "../../services/APIClient";
import { usePeopleContext } from "../contexts/People.context";
import { DEFAULT_PAGE_SIZE } from "../molecules/ListHeader";
import { DISPLAY_MODE_CARD, SettingsDisplayMode } from "../../constants/types";

export const usePeople = () => {
  const [currentPage, setCurrentPage] = useState(0);
  const [pageSize, setPageSize] = useState(() => {
    try {
      const size = localStorage.getItem("@BEON:pageSize");

      return size ? Number(size) : DEFAULT_PAGE_SIZE;
    } catch (e) {
      return DEFAULT_PAGE_SIZE;
    }
  });
  const [displayMode, setDisplayMode] = useState<SettingsDisplayMode>(() => {
    try {
      const mode = localStorage.getItem("@BEON:displayMode") as SettingsDisplayMode;

      return mode || DISPLAY_MODE_CARD;
    } catch (e) {
      return DISPLAY_MODE_CARD;
    }
  });
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const peopleData = usePeopleContext();

  // The following hook (useCallback) may not be neccesary
  // just showing a possible use case
  const execute = useCallback(
    async (size = 20) => {
      console.log("its been called", size);
      setLoading(true);
      try {
        const { data, totalItems } = await APIClient.getPeopleInfo({
          quantity: size,
          page: currentPage,
        });
        setCurrentPage(currentPage + 1);
        if (peopleData.initialized) peopleData.append(data, totalItems);
        else peopleData.initialize(data, totalItems);
      } catch (err) {
        setCurrentPage(0);
        setError(error);
      } finally {
        setLoading(false);
      }
    },
    [peopleData.initialize]
  );

  return {
    ...peopleData,
    execute,
    error,
    loading,
    pageSize,
    setPageSize,
    displayMode,
    setDisplayMode,
  };
};
