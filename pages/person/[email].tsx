import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import {
  Button,
  PageHeader,
  Descriptions,
  Input,
  Select,
  message,
  Form,
} from "antd";

import { withContextInitialized } from "../../components/hoc";
import CompanyCard from "../../components/molecules/CompanyCard";
import GenericList from "../../components/organisms/GenericList";
import OverlaySpinner from "../../components/molecules/OverlaySpinner";
import { usePersonInformation } from "../../components/hooks/usePersonInformation";

import { Company } from "../../constants/types";
import { ResponsiveListCard } from "../../constants";

const labelStyle = { width: "65px" };

const PersonDetail = () => {
  const router = useRouter();
  const [form] = Form.useForm();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [isEditting, setIsEditting] = useState(false);

  const handleEditting = () => {
    setIsEditting((state) => {
      if (state) form.resetFields();
      return !state;
    });
  };

  const saveForm = async (values) => {
    await save({
      ...data,
      ...values,
    });
    setIsEditting(false);
  };

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return (
      <OverlaySpinner title={`Loading ${router.query?.email} information`} />
    );
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push("/home")
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
            key="go-to-website"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={handleEditting} key="edit-button">
            {isEditting ? "Cancel" : "Edit"}
          </Button>,
        ]}
      >
        {data && (
          <Form
            onFinish={(values) => saveForm(values)}
            form={form}
            initialValues={{
              name: data?.name,
              phone: data?.phone,
              birthday: data?.birthday,
              gender: data?.gender,
            }}
          >
            <Descriptions size="small" column={1}>
              <Descriptions.Item label="Name" labelStyle={labelStyle}>
                <Form.Item name="name" rules={[{ required: true }]} noStyle>
                  <Input disabled={!isEditting} />
                </Form.Item>
              </Descriptions.Item>
              <Descriptions.Item label="Gender" labelStyle={labelStyle}>
                <Form.Item name="gender" rules={[{ required: true }]} noStyle>
                  <Select disabled={!isEditting}>
                    <Select.Option value="male">Male</Select.Option>
                    <Select.Option value="female">Female</Select.Option>
                  </Select>
                </Form.Item>
              </Descriptions.Item>
              <Descriptions.Item label="Phone" labelStyle={labelStyle}>
                <Form.Item name="phone" rules={[{ required: true }]} noStyle>
                  <Input disabled={!isEditting} />
                </Form.Item>
              </Descriptions.Item>

              <Descriptions.Item label="Birthday" labelStyle={labelStyle}>
                <Form.Item name="birthday" rules={[{ required: true }]} noStyle>
                  <Input disabled={!isEditting} />
                </Form.Item>
              </Descriptions.Item>
            </Descriptions>
            {isEditting && (
              <Button type="primary" htmlType="submit">
                Save
              </Button>
            )}
          </Form>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
